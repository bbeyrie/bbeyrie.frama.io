---
title: "MIT Blog"
date: 2022-09-10T00:00:00
image: "images/portfolio/MIT.png"
categories: ["blog"]
description: "MIT Blog"
draft: false
project_info:
- name: "MIT Blog"
  icon: "fas fa-link"
  content: "https://mitgovlab.org/news/"
---

The latest from MI research lab