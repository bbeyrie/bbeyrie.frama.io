---
title: "Kaggle"
date: 2022-09-10T00:00:00
image: "images/portfolio/Kaggle.png"
categories: ["code", "challenge"]
description: "Coding competition"
draft: false
project_info:
- name: "Kaggle"
  icon: "fas fa-link"
  content: "https://www.kaggle.com/"
---

Coding competition and learning