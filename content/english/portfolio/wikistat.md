---
title: "Wikistat"
date: 2022-09-10T00:00:00
image: "images/portfolio/Wikistat.jpg"
categories: ["blog", "code", "course", "repository"]
description: "Wikistat courses"
draft: false
project_info:
- name: "Wikistat website"
  icon: "fas fa-link"
  content: "http://wikistat.fr/"
- name: "Wikistat repository"
  icon: "fas fa-link"
  content: "https://github.com/wikistat"
---

Data Scientist Courses from Toulouse Academy