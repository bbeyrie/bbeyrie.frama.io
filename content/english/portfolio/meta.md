---
title: "Meta AI Blog"
date: 2022-09-10T00:00:00
image: "images/portfolio/MetaAI.png"
categories: ["blog"]
description: "Meta AI Blog"
draft: false
project_info:
- name: "Meta AI Blog"
  icon: "fas fa-link"
  content: "https://ai.facebook.com/blog/"
---

The latest from Meta research