---
title: "Google AI Blog"
date: 2022-09-10T00:00:00
image: "images/portfolio/GoogleAI.png"
categories: ["blog"]
description: "Google AI Blog"
draft: false
project_info:
- name: "Google AI Blog"
  icon: "fas fa-link"
  content: "https://ai.googleblog.com/"
---

The latest from google research