---
title: "Open AI Blog"
date: 2022-09-10T00:00:00
image: "images/portfolio/OpenAI.png"
categories: ["blog"]
description: "Open AI Blog"
draft: false
project_info:
- name: "Open AI Blog"
  icon: "fas fa-link"
  content: "https://openai.com/blog/"
---

The latest from OpenAI research