---
title: "Towards Data Science"
date: 2022-09-10T00:00:00
image: "images/portfolio/Towardsdatascience.jpg"
categories: ["blog"]
description: "Towards Data Science"
draft: false
project_info:
- name: "Towards Data Science"
  icon: "fas fa-link"
  content: "https://towardsdatascience.com/"
---

Data Science blog where anybody can post, sometime there are usefull content