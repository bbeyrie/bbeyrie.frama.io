---
title: "Microsoft AI Blog"
date: 2022-09-10T00:00:00
image: "images/portfolio/MicrosoftAI.png"
categories: ["blog"]
description: "Microsoft AI Blog"
draft: false
project_info:
- name: "Microsoft AI Blog"
  icon: "fas fa-link"
  content: "https://www.microsoft.com/en-us/research/blog/"
---

The latest from microsoft research