---
title : "KNOW MORE <br> ABOUT ME"
image : "images/backgrounds/benben.png"
# button
button:
  enable : true
  label_fr : "DOWNLOAD MY CV (FR)"
  label_en : "DOWNLOAD MY CV (EN)"
  link_fr : "resume/Benjamin_Beyrie_CV.pdf"
  link_en : "resume/Benjamin_Beyrie_CV_en.pdf"

########################### Experience ##############################
experience:
  enable : true
  title : "EXPERIENCES"
  experience_list:
    # experience item loop
    - name : "Consultant Data Scientist"
      company : "Astek"
      duration : "02/2021 – 07/2022"
      content : "Mission for Pierre Fabre, within the CED (Center of Excellence for Data)"
      
    # experience item loop
    - name : "Consultant Data Scientist"
      company : "Umlaut Company (P3 Group)"
      duration : "12/2018 – 12/2020"
      content : "Missions for Airbus branch Aeronautics, IZC department (Configuration Management)"
      
    # experience item loop
    - name : "Junior Data Scientist"
      company : "CS Communication & Systèmes"
      duration : "02/2018 - 08/2018 – Internship"
      content : "Analysis of multitemporal satellite images by constrained clustering"

    # experience item loop
    - name : "Junior Data Scientist"
      company : "Laboratoires LAPLACE ENSEEIHT & LAAS"
      duration : "03/2017 - 07/2017  – Internship"
      content : "Predictive maintenance on bicycles / Machine Learning"

########################### Education ##############################
education:
  enable : true
  title : "EDUCATION"
  education_list:

    # experience item loop
    - name : "Master degree MAPI³ - Mathématiques appliquées pour l'Ingénierie, l'Industrie et l'Innovation"
      school : "Université Paul Sabatier - Toulouse III"
      duration : "2016-2018"
      content : "Applied mathematics, https://departement-math.univ-tlse3.fr/mapi3"

    # experience item loop
    - name : "Licence degree MAPI³ - Mathématiques appliquées pour l'Ingénierie, l'Industrie et l'Innovation"
      school : "Université Paul Sabatier - Toulouse III"
      duration : "2011-2015"
      content : "Applied mathematics, https://departement-math.univ-tlse3.fr/l3-mapi3"

    # experience item loop
    - name : "Baccalauréat scientifique"
      school : "Lycée polyvalent Le Garros"
      duration : "2009-2011"
      content : "High school diploma in science"

############################### Skill #################################
skill:
  enable : true
  title : "SKILLS"
  skill_list:
    # skill item loop
    - name : "Python"
      level : "Advanced"
      percentage : "80%"

    # skill item loop
    - name : "SQL"
      level : "Medium"
      percentage : "50%"

    # skill item loop
    - name : "R"
      level : "Basics"
      percentage : "20%"
      
    # skill item loop
    - name : "Data Science"
      level : "Good"
      percentage : "65%"
      
    # skill item loop
    - name : "Data Engineering"
      level : "Medium"
      percentage : "50%"
      
    # skill item loop
    - name : "Data Visualisation / BI"
      level : "Medium"
      percentage : "50%"

    # skill item loop
    - name : "Data Mining / Webscraping"
      level : "Advanced"
      percentage : "80%"

############################### Tool #################################
tool:
  enable : true
  title : "TOOLS"
  tool_list:
    # skill item loop
    - name : "Alteryx"

    # skill item loop
    - name : "Anaconda Suite"

    # skill item loop
    - name : "DBeaver"

    # skill item loop
    - name : "Git"

    # skill item loop
    - name : "Kubeflow"

    # skill item loop
    - name : "Skywise"

    # skill item loop
    - name : "Snowflake"

      # skill item loop
    - name : "Tableau Software"


# custom style
custom_class: "" 
custom_attributes: "" 
custom_css: ""
---

I'm Benjamin BEYRIE, a 29 years old data scientist living in Toulouse in the south of France.