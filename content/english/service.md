---
title : "SERVICES"
service_list:
# service item loop
- name : "Web Scraping"
  image : "images/icons/web-development.png"
  
# service item loop
- name : "Data Visualisation"
  image : "images/icons/graphic-design.png"

# service item loop
- name : "Data Science"
  image : "images/icons/datascience.png"

# service item loop
- name : "Data Management"
  image : "images/icons/dbms.png"
  
# service item loop
- name : "Python Development"
  image : "images/icons/software-development.png"

# service item loop
- name : "Multitask"
  image : "images/icons/works.png"




# custom style
custom_class: "" 
custom_attributes: "" 
custom_css: ""
---