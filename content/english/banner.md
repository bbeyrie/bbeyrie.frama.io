---
title : "I'm Benjamin Beyrie"
# full screen navigation
first_name : "Benjamin"
last_name : "BEYRIE"
bg_image : "images/slider/goutte.gif"
# animated text loop
occupations:
- "Data Scientist"
- "Data Enthusiast"
- "Python Lover"

# slider background image loop
slider_images:
- "images/slider/midjourney4.jpg"
- "images/slider/midjourney1.jpg"
- "images/slider/midjourney3.jpg"

# button
button:
  enable : true
  label : "HIRE ME"
  link : "#contact"


# custom style
custom_class: "" 
custom_attributes: "" 
custom_css: ""

---